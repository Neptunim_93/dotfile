if command -v trans &>/dev/null; then
	alias en='trans -b fr:en'
	alias fr='trans -b en:fr'
fi
